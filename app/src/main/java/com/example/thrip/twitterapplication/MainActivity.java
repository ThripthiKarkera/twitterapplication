package com.example.thrip.twitterapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import static android.R.attr.data;

public class MainActivity extends AppCompatActivity {

    private TwitterLoginButton loginButton;
    private TextView tv_status;
    private static final String  CONSUMER_KEY="VlIaKBjG8AYbcDOUbKr4hDBK4";
    private static final String  CONSUMER_KEY_SECRET="4MW0ajENWR8IaFzpivBZH4yIwBDrvNwVoHZnoBR26gq4JM8RqR";
    private Button bt_compose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(CONSUMER_KEY,CONSUMER_KEY_SECRET))
                .debug(true)
                .build();
        Twitter.initialize(config);
        setContentView(R.layout.activity_main);

        loginButton = (TwitterLoginButton) findViewById(R.id.login_button);
        tv_status= (TextView) findViewById(R.id.tv_status);
        bt_compose= (Button) findViewById(R.id.bt_compose);
        bt_compose.setVisibility(View.GONE);


        loginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(final Result<TwitterSession> twitterSessionResult) {
                // Do something with result, which provides a TwitterSession for making API calls
                TwitterAuthClient authClient = new TwitterAuthClient();
                authClient.requestEmail(twitterSessionResult.data, new Callback<String>() {
                    @Override
                    public void success(Result<String> result) {

                        String output = "Status: " +  "Your login was successful " +twitterSessionResult.data.getUserName() +"\nAuth Token Received: " +   twitterSessionResult.data.getAuthToken().token;
                        tv_status.setText(output);
                        bt_compose.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure(TwitterException exception) {
                        Log.i("Failed", exception.getMessage());
                    }
                });
            }

            @Override
            public void failure(TwitterException exception) {
                Log.i("Failed", exception.getMessage());
            }
        });


        bt_compose.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
          composeTweets();
         }
        });
    }


        @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result to the login button.
        loginButton.onActivityResult(requestCode, resultCode, data);
    }


    public void composeTweets()
    {
         TweetComposer.Builder builder = new TweetComposer.Builder(this)
                 .text("just setting up my Twitter Kit.");
         builder.show();
       //     .image(imageUri);

    }
}
